var uuidV4 = require('uuid/v4');
var fs = require('fs');

var node_list = [
    {id: uuidV4(), name: 'Node_1'},
    {id: uuidV4(), name: 'Node_2'},
    {id: uuidV4(), name: 'Node_3'},
    {id: uuidV4(), name: 'Node_4'},
    {id: uuidV4(), name: 'Node_5'}
];

var nodes = {};
for(var i = 0; i < node_list.length; i++) {
    var id = node_list[i].id;
    nodes[id] = {
        id: id,
        name: node_list[i].name,
        peers: [],
        rumors: [],
        all_rumors: {},
        endpoint: 'http://ec2-52-35-19-183.us-west-2.compute.amazonaws.com:3002/message/' + id
    };
    while (nodes[id].peers.length < 2) {
        var index = Math.floor(Math.random()*node_list.length);
        var peer_id = node_list[index].id;
        if (id == peer_id || nodes[id].peers.indexOf(peer_id) > -1) {
            continue;
        }
        nodes[id].peers.push(peer_id);
    }
}

fs.writeFileSync('nodes.json', JSON.stringify({node_list: node_list, nodes: nodes}));
