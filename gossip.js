var express = require('express');
var app = express();
var axios = require('axios');
var bodyParser = require('body-parser');
var fs = require('fs');
var uuidV4 = require('uuid/v4');

var Nodes = JSON.parse(fs.readFileSync('./setup/nodes.json', 'utf8'));;
var nodes = Nodes.nodes;

app.use(bodyParser.json());

var urlEncodedParser = bodyParser.urlencoded({ extended: false });

app.use(express.static(__dirname + '/public'));

app.get('/', function (req, res) {
    res.sendFile(__dirname + '/public/gossip/landing.html');
});

app.get('/nodes', function(req, res) {
    res.send(JSON.stringify(Nodes.node_list));
});

app.get('/node/:id', function(req, res) {
    res.sendFile(__dirname + '/public/gossip/chatroom.html');
});

app.post('/node/:id/add', urlEncodedParser, function(req, res) {
    var id = req.params.id;
    var newNode = req.body.name;
    var endpoint = req.body.endpoint;
    var uuid = req.body.uuid;
    var response = addNewNode(id, newNode, endpoint, uuid);
    if (!response) {
        res.sendStatus(404);
    } else {
        res.sendStatus(200);
    }
});

function addNewNode(id, name, endpoint, uuid) {
    if(!nodes[id]) {
        return null;
    }
    if(nodes[id].peers.indexOf(uuid) === -1) {
        nodes[id].peers.push(uuid);
    }
    if(!nodes[id].all_rumors[uuid]) {
        nodes[id].all_rumors[uuid] = [];
    }
    if(!nodes[uuid])
    {
        nodes[uuid] = {
            id: uuid,
            name: name,
            endpoint: endpoint
        };
    }
    return true;
}

app.get('/messages/:id', function(req, res) {
    if(nodes[req.params.id]) {
        var curNode = nodes[req.params.id];
        var responseArr = [];
        for(var i = 0; i < curNode.rumors.length; i++) {
            var rumId = curNode.rumors[i].split(':');
            responseArr.push(curNode.all_rumors[rumId[0]][parseInt(rumId[1])]);
        }
        res.send(responseArr);
    } else {
        res.send('No available nodes with id:' + req.params.id);
    }
});

app.post('/message', urlEncodedParser, function(req, res) {
    var id = req.body && req.body.node_id;
    var message = req.body && req.body.message;
    if(!nodes[id]) {
        res.sendStatus(404);
        return;
    }
    if(!nodes[id].all_rumors[id]) {
        nodes[id].all_rumors[id] = [];
    }
    var seq = nodes[id].all_rumors[id].length;
    var rumor = {
        Rumor: {
            MessageID: id + ':' + seq,
            Originator: nodes[id].name,
            Text: message
        },
        EndPoint: nodes[id].endpoint
    };
    nodes[id].all_rumors[id].push(rumor);
    nodes[id].rumors.push(id+':'+seq);
    res.sendStatus(200);
});

app.post('/message/:id', urlEncodedParser, function(req, res) {
    var id = req.params.id;
    var msg = req.body;
    if(!msg || !nodes[id]) {
        res.sendStatus(404);
        return;
    }

    if(msg.Rumor) {
        var messageID = msg.Rumor && msg.Rumor.MessageID;
        var sender_id = messageID.split(':')[0];
        var seq = parseInt(messageID.split(':')[1]);
        if(!nodes[id].all_rumors[sender_id]) {
            nodes[id].all_rumors[sender_id] = [];
        }
        if(nodes[id].peers.indexOf(sender_id) === -1) {
            nodes[id].peers.push(sender_id);
        }
        addNewNode(id, msg.Rumor.Originator, msg.Endpoint, sender_id);
        if(sender_id !== id && seq === nodes[req.params.id].all_rumors[sender_id].length) {
            nodes[req.params.id].all_rumors[sender_id].push(msg);
            nodes[req.params.id].rumors.push(messageID);
        }

    } else if(msg.Want) {
        Object.keys(msg.Want).forEach(function(key){
            var seq = msg.Want[key];
            if(nodes[id].all_rumors[key] && nodes[id].all_rumors[key].length > seq + 1) {
                for(var i = seq + 1; i < nodes[id].all_rumors[key].length; i++){
                    axios.post(msg.EndPoint, nodes[id].all_rumors[key][i]);
                }
            }
        });
    }
    res.sendStatus(200);
});

var generateRandomMessage = function(node) {
    var isRumor = Math.floor(Math.random()*2);
    var msg = {};
    if(isRumor && node.rumors.length > 0) {
        msgID = node.rumors[Math.floor(Math.random()*node.rumors.length)];
        var id = msgID.split(':')[0];
        var seq = parseInt(msgID.split(':')[1]);
        return node.all_rumors[id][seq];
    } else {
        msg['Want'] = {};
        Object.keys(node.all_rumors).forEach(function(key){
            if(key !== node.id) {
                msg['Want'][key] = node.all_rumors[key].length - 1;
            }
        });
        msg['EndPoint'] = node.endpoint;
        if(Object.keys(msg.Want).length === 0) {
            return null;
        }
    }
    return msg;
};


var cronJob = function() {
    for(var i = 0; i < Nodes.node_list.length; i++) {
        var curNode = nodes[Nodes.node_list[i].id];
        var peer_id = curNode.peers[Math.floor(Math.random()*curNode.peers.length)];
        var peer_url = nodes[peer_id].endpoint;
        var message = generateRandomMessage(curNode);
        if(!message) { continue; }
        axios.post(peer_url, message).catch(function(err) {
            //Error sending message to peer
        });
    }
};

setInterval(cronJob, 1000);


app.listen(3002, function () {
    console.log('Example app listening on port 3002!');
});
