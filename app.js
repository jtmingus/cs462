var express = require('express');
var app = express();
// var https = require('https');
var axios = require('axios');
var fs = require('fs');
var cookieParser = require('cookie-parser');

var config = {
  'secrets' : {
    'clientId' : 'FPHFJ3PPG54MNCT1MS3CDYM5O0SOOIPINAD0KKQQ5W2VNNEI',
    'clientSecret' : '2NZS0M43YW3TTEKEFLV5NN1SU4YYYVKRDBETPNLHQZ3NONUA',
    'redirectUrl' : 'https://foursquare.iolplus.com/redirect'
  }
};

var foursquare = require('node-foursquare')(config);

var accessToken;


var userData = { users: {} };
var saveUserData = function() {
//    console.log("BEFORE", userData.users);
    fs.writeFileSync('userData.json', JSON.stringify(userData));
//    console.log("AFTER", userData.users);
};

var userData = JSON.parse(fs.readFileSync('userData.json', 'utf8'));
//console.log("START:", userData.users);
var sessions = {};




//https.createServer(sslOptions, app).listen(3000)
app.use(cookieParser());
app.use(express.static(__dirname + '/public'));

app.get('/', function (req, res) {
  res.sendFile(__dirname + '/public/landing.html');
});

app.get('/login', function(req, res) {
  res.writeHead(303, { 'location': foursquare.getAuthClientRedirectUrl() });
  res.end();
});

app.get('/logout', function(req, res) {
    res.clearCookie('Session-id');
    res.clearCookie('User-id');
    res.end();
});

app.get('/redirect', function (req, res) {
  foursquare.getAccessToken({
    code: req.query.code
}, function (error, _accessToken) {
    if(error) {
      res.send('An error was thrown: ' + error.message);
    }
    else {
		accessToken = _accessToken;
        res.cookie('Session-id', accessToken);
        res.redirect('/profile');
    }
  });
});

app.get('/profile', function(req, res) {
    res.sendFile(__dirname + '/public/profile.html');
});

app.get('/profile/:id', function(req, res) {
    res.sendFile(__dirname + '/public/profile.html');
})

app.get('/checkins/:id', function(req, res) {
    axios.get('https://api.foursquare.com/v2/users/' + req.params.id + '/checkins', {
		params: {
			oauth_token: req.cookies['Session-id'],
			v: 20170211
		}
	})
	.then(function(response) {
		res.send(JSON.stringify(response.data.response));
	})
	.catch(function(error) {
		if(userData.users[req.params.id] && userData.users[req.params.id].checkins) {
			res.send(JSON.stringify({'checkins': userData.users[req.params.id].checkins}));
		} else {
			res.status(404).send('ERROR:' + error);
		}
	});
});

app.get('/user', function(req, res) {
    console.log(req.cookies, req.query.id)
    if(!req.cookies['Session-id'] || (req.cookies['User-id'] && req.cookies['User-id'] !== req.query.id)) {
        if(userData.users[req.query.id]) {
            res.send(JSON.stringify({user: userData.users[req.query.id]}));
        } else {
            res.status(404).send('No data available');
        }
    } else {
    	axios.get('https://api.foursquare.com/v2/users/self', {
    		params: {
    			oauth_token: req.cookies['Session-id'],
    			v: 20170211
    		}
    	})
    	.then(function(response) {
            var data = response.data.response;
            userData.users[data.user.id] = JSON.parse(JSON.stringify(data.user));
    	    saveUserData();
            res.cookie('User-id', data.user.id);
    		res.send(JSON.stringify(data));
    	})
    	.catch(function(error) {
    	    res.status(404).send('ERROR:' + error);
    	});
    }
});

app.get('/users', function(req, res) {
    var users = [];
    for(var key in userData.users) {
        var curUser = {};
        curUser.id = key;
        curUser.name = userData.users[key].firstName + ' ' + userData.users[key].lastName;
        users.push(curUser);
    }
    res.send(JSON.stringify(users));
});

app.listen(3001, function () {
  console.log('Example app listening on port 3001!');
});
